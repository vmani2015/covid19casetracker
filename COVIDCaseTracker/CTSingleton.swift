//
//  CTSingleton.swift
//  COVIDCaseTracker
//
//  Created by Vin on 4/9/21.
//  Copyright © 2021 Vin. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import CSV
import Kanna
import FMDB
import Reachability


class CTSingleton {
    
    let stats_site_website_link = "https://www.covid19.act.gov.au/"
    let exposure_site_website_link = "https://www.covid19.act.gov.au/act-status-and-response/act-covid-19-exposure-locations"
    let csv_file_base_link = "https://www.covid19.act.gov.au/__data"
    var csv_file_web_path = ""
    var file_name = ""
    var csv_file_local_path = ""
    var Exposure_Sites = [ExposureSite]()
    var processing_Queue = DispatchQueue(label: "pq")
    let loading_layer = CAShapeLayer()
    let animation_duration = 10
    
    
    let loading_screen_label = UILabel()
    let subtitle_label = UILabel()
    let new_case_label = UILabel()
    let active_case_label = UILabel()
    let negative_test_result_label = UILabel()
    let total_vaccinations_label = UILabel()
    
    var loading_label_start_value = 0
    let loading_label_end_value = 10000
    
    var daily_new_cases = 0
    var active_cases = 0
    var negative_test_results_in_24_hrs = 0
    var total_vaccinations = 0
    var last_updated = ""
    
    var new_daily_case_start_value = 300
    var active_case_start_value = 500
    var negative_test_result_start_value = 3000
    var total_vacinations_start_value = 220000
    
    var db_queue = FMDatabaseQueue(path: "test")
    
    let default_file_path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    
    var current_db = FMDatabase(path: "")
    
    var get_value_from_db = false
    
    let network_connection:Reachability?
    
    var internet_working = false
    
    
    private init()
    {
        do {
            try network_connection = Reachability(hostname: "Test")
        } catch  {
            print("Network connection not initialised properly")
            network_connection = nil
        }
//        createandOpenDatabase()
//        startReachability()
    }
    
    func startReachability()
    {
        network_connection!.whenReachable = { reachability in
            if self.network_connection!.connection == .wifi {
                print("Reachable via WiFi")
                self.internet_working = true
            } else if self.network_connection!.connection == .cellular {
                print("Reachable via Cellular")
                self.internet_working = true
            }
        }
        network_connection!.whenUnreachable = { _ in
            print("Not reachable")
          //  self.internet_working = false
        }
        
        do {
            try network_connection!.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    func isConnectedtoInternet()->Bool
    {
        var connected_to_internet = false
        
        if (network_connection!.connection == .wifi)
        {
            connected_to_internet = true
        }
        else if (network_connection!.connection == .cellular)
        {
            connected_to_internet = true
        }
        
        return connected_to_internet
    }
    
    func stopReachability()
    {
        network_connection!.stopNotifier()
    }
    

     
    static let shared = CTSingleton()
    
    func closeDatabase()
    {
        // Close the database connection
        guard current_db.close() else {
            print("Unable to close database")
            return
        }
    }
    
    func getCurrentDateStringInFormat(p_format:String = "dd MMM yyyy")->String
    {
        let dateObj = Date()
        let datetformatter = DateFormatter()
        datetformatter.timeZone = TimeZone.current
        datetformatter.dateFormat = p_format
        let date_string = datetformatter.string(from: dateObj)
        print("DATE HERE: \(date_string)")
        return date_string
    }
    
    func printAllRowsFromTable()
    {
        let select_statement = "SELECT * FROM IMPORTANT_STATS"
        
        do {
            let rs = try current_db.executeQuery(select_statement, values: nil)
            while rs.next() {
                print(rs.string(forColumn: "STAT_NAME"))
                print(rs.string(forColumn: "STAT_TEXT"))
            }
        } catch  {
            print("Error here")
        }
        
        
    }
    
    func getStatTextForStatName(p_stat_name:String) -> String
    {
        let select_statement = "SELECT STAT_TEXT FROM IMPORTANT_STATS WHERE STAT_NAME = '\(p_stat_name)' "
        var return_string = ""
        
        do {
            let rs = try current_db.executeQuery(select_statement, values: nil)
            while rs.next() {
                return_string = rs.string(forColumn: "STAT_TEXT")!
            }
        } catch  {
            print("Error here")
        }
        
        return return_string
        
    }
    
    func updateRowInTable(stat_name:String,stat_text:String)->Bool
    {
        let updateValues_sql = "UPDATE IMPORTANT_STATS SET STAT_TEXT = '\(stat_text)' WHERE STAT_NAME = '\(stat_name)';"
        let result = current_db.executeStatements(updateValues_sql)
        
        if (result)
        {
            print("Value successfully updated")
        }
        else
        {
            print("Value not updated")
        }
        
        return result
        
    }
    
    func insertRowInTable(stat_name:String,stat_text:String)->Bool
    {
        let insertValues_sql = "INSERT INTO IMPORTANT_STATS (STAT_NAME,STAT_TEXT) VALUES ('\(stat_name)','\(stat_text)');"
        let result = current_db.executeStatements(insertValues_sql)
            
        if (result)
        {
            print("Value successfully inserted")
        }
        else
        {
            print("Value not inserted")
        }
        
        return result
        
    }
    
    func createandOpenDatabase()
    {
        //Create the database if it doesn't exist
        let db_name = "CT.db"
        let db_path_string = default_file_path.appendingPathComponent(db_name)
        current_db = FMDatabase(url: db_path_string)
        
        
        // Open the database connection
        guard current_db.open() else {
            print("Unable to open database")
            return
        }
        
        do
        {
            // Create the table that we need
            let create_table_sql = "CREATE TABLE IF NOT EXISTS IMPORTANT_STATS(STAT_NAME TEXT,STAT_TEXT TEXT);"
//            let create_table_sql = "CREATE TABLE IMPORTANT_STATS(ID INTEGER PRIMARY KEY AUTOINCREMENT,STAT_NAME TEXT,STAT_TEXT TEXT);"
//            let insertLastUpdatedDate_sql = "INSERT INTO IMPORTANT_STATS (STAT_NAME,STAT_TEXT) VALUES ('TEST','TEST1');"
//            let insertLastUpdatedDate_sql = "INSERT INTO IMPORTANT_STATS (STAT_NAME,STAT_TEXT) VALUES ('LAST_UPDATED_DATE','\(CTSingleton.shared.last_updated)');"
            if (current_db.executeStatements(create_table_sql) == true)
            {
                print("IMPORTANT_STATS table created")
            }
            else
            {
                //print("IMPORTANT_STATS table not created")
                print(current_db.lastErrorMessage())
            }
//            if (current_db.executeStatements(insertLastUpdatedDate_sql) == true)
//            {
//                print("Value successfully inserted")
//            }
//            else
//            {
//                print("Value not successfully inserted")
//            }
//            let select_statement = "SELECT * FROM IMPORTANT_STATS"
//            let rs = try current_db.executeQuery(select_statement, values: nil)
//            while rs.next() {
//                print(rs.string(forColumn: "STAT_NAME"))
//                print(rs.string(forColumn: "STAT_TEXT"))
//            }
        }
        catch
        {
            print("Error Here")
        }
        
        //current_db.close()
        
        
        
        
    }
    
    
    
    func updateLastUpdatedDateinDatabase()
    {
        //Create the database if it doesn't exist
        let db_name = "CT.db"
        let db_path_string = default_file_path.appendingPathComponent(db_name)
        current_db = FMDatabase(url: db_path_string)
        
        
        
        
        
        // Open the database connection
        guard current_db.open() else {
            print("Unable to open database")
            return
        }
        
        do
        {
            // Create the table that we need
            let create_table_sql = "CREATE TABLE IMPORTANT_STATS(ID INTEGER PRIMARY KEY AUTOINCREMENT,STAT_NAME TEXT,STAT_TEXT TEXT);"
            //            let insertLastUpdatedDate_sql = "INSERT INTO IMPORTANT_STATS (STAT_NAME,STAT_TEXT) VALUES ('TEST','TEST1');"
            let insertLastUpdatedDate_sql = "INSERT INTO IMPORTANT_STATS (STAT_NAME,STAT_TEXT) VALUES ('LAST_UPDATED_DATE','\(CTSingleton.shared.last_updated)');"
            if (current_db.executeStatements(create_table_sql) == true)
            {
                print("Database created")
            }
            else
            {
                print("Database not created")
            }
            if (current_db.executeStatements(insertLastUpdatedDate_sql) == true)
            {
                print("Value successfully inserted")
            }
            else
            {
                print("Value not successfully inserted")
            }
            let select_statement = "SELECT * FROM IMPORTANT_STATS"
            let rs = try current_db.executeQuery(select_statement, values: nil)
            while rs.next() {
                print(rs.string(forColumn: "STAT_NAME"))
                print(rs.string(forColumn: "STAT_TEXT"))
            }
        }
        catch
        {
            print("Error Here")
        }
        
        current_db.close()
        
        
        
        
    }
    
    func getLocalFilePath()->URL
    {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileURL = documentsURL.appendingPathComponent(CTSingleton.shared.file_name)
        return fileURL
        
    }
    
    func processCOVID19Stats(html_input:String)
    {
        
    }
    
    
    
    func downloadCSVFile(completionhandler:@escaping ()->Void) -> Void
    {
        self.getPathOfCSVFile {
            
            let start_index_file_path =
                CTSingleton.shared.csv_file_web_path.index(CTSingleton.shared.csv_file_web_path.startIndex, offsetBy: self.getSubStingStartIndex(string_input: CTSingleton.shared.csv_file_web_path, sub_string_input: "Exposure"))
            let end_index_file_path = CTSingleton.shared.csv_file_web_path.index(CTSingleton.shared.csv_file_web_path.endIndex, offsetBy: -1)
            CTSingleton.shared.file_name = String(CTSingleton.shared.csv_file_web_path[start_index_file_path...end_index_file_path])
            print("Excel file called : \(CTSingleton.shared.file_name)")
            
            let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                
                //                print(fileURL)
                
                //            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
                
                //                return (fileURL,[])
                
                return (self.getLocalFilePath(),[])
            }
            
            
            
            if (self.fileExists(p_file_name: CTSingleton.shared.file_name) == false)
            {
                
                Alamofire.download(CTSingleton.shared.csv_file_web_path, to: destination).response { response in
                    //                 debugPrint(response)
                    
                    if response.error == nil {
                        print("Success")
                        completionhandler()
                    }
                }
                
            }
            else
            {
                completionhandler()
            }
            
        }
        
        
    }
    
    func fileExists(p_file_name:String) -> Bool
    {
        var return_option = false
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        if let pathComponent = url.appendingPathComponent(p_file_name) {
            let filePath = pathComponent.path
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: filePath) {
                print("FILE AVAILABLE")
                return_option = true
            } else {
                print("FILE NOT AVAILABLE")
            }
        } else {
            print("FILE PATH NOT AVAILABLE")
        }
        
        return return_option
    }
    
    func getSubStingStartIndex(string_input:String,sub_string_input:String) ->Int
    {
        var val = 0
        
        if let range: Range<String.Index> = string_input.range(of: sub_string_input) {
            val = string_input.distance(from: sub_string_input.startIndex, to: range.lowerBound)
            print("index: ", val) //index: 2
        }
        else {
            print("substring not found")
            val = -1
        }
        return val
    }
    
    func getDailyCOVID19Stats(completionhandler:@escaping ()->Void) -> Void {
        // If the programme is run at ant time other than between and 11am and 2pm one day
        /*if(Int(getCurrentDateStringInFormat(p_format: "H"))! < 11 || Int(getCurrentDateStringInFormat(p_format: "H"))! > 14)
        {
            print("Values in DB")
            CTSingleton.shared.get_value_from_db = true
            completionhandler()
        }
        //First check if the values are in the DB
        else if ( dbUpdatedToday())
        {
          print("Values in DB")
            CTSingleton.shared.get_value_from_db = true
            completionhandler()
        }
        // Then check if the internet connection is invalid
        else*/ if ( CTSingleton.shared.internet_working == false)
        {
            CTSingleton.shared.get_value_from_db = true
            completionhandler()
        }
        else
        {
            Alamofire.request(CTSingleton.shared.stats_site_website_link).responseString { response in
                print("REQUEST FOR STATS IS : \(response.result.isSuccess)")
                if let html_input = response.result.value {
                    //print(response.result.value)
                    //self.parseHTML(html_input: html)
                    //let start_index_here = self.getSubStingStartIndex(string_input: html_input, sub_string_input: "Papa.")
                    //let end_index_here = self.getSubStingStartIndex(string_input: html_input, sub_string_input: ".csv")
                    //let test1 = html_input.index(html_input.startIndex, offsetBy: start_index_here + 12)
                    //let test2 = html_input.index(html_input.startIndex, offsetBy: start_index_here + (end_index_here - start_index_here + 3))
                    //CTSingleton.shared.csv_file_web_path = String(html_input[test1...test2])
                    //completionhandler(response.result.value!)
                    do
                    {
                        let doc = try Kanna.HTML(html: response.result.value!, encoding: String.Encoding.utf8)
                        //                    for container in doc.css("div[class^='contentContainer'")\
                        var temp_string = ""
                        
                        for show in doc.css("p[class^='stat-card-text']")
                        {
                            
                            // Remove all commas in eahc number
                            temp_string = (show.previousSibling?.content)!
                            temp_string = temp_string.replacingOccurrences(of: ",", with: "")
                            
                            
                            if (show.content! == "New cases")
                            {
                                CTSingleton.shared.daily_new_cases = Int(temp_string)!
                            }
                            else if (show.content! == "Active cases")
                            {
                                CTSingleton.shared.active_cases =  Int(temp_string)!
                            }
                            else if (show.content! == "Negative test results in the last 24 hours")
                            {
                                CTSingleton.shared.negative_test_results_in_24_hrs  = Int(temp_string)!
                            }
                            else if (show.content! == "Total vaccinations*")
                            {
                                CTSingleton.shared.total_vaccinations  = Int(temp_string)!
                            }
                            
                            
                        }
                        
                        self.last_updated = (doc.css("div[class^='spf-article-card--tabular-subtitle']").first?.content)!
                        self.last_updated = self.last_updated.replacingOccurrences(of: "\r", with: "")
                        self.last_updated = self.last_updated.replacingOccurrences(of: "\n", with: "")
                        self.last_updated = self.last_updated.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                        
                        
                        
                        print(self.last_updated)
                        print(self.daily_new_cases)
                        print(self.active_cases)
                        print(self.negative_test_results_in_24_hrs)
                        print(self.total_vaccinations)
                        
                        
                    }
                    catch
                    {
                        
                    }
                    completionhandler()
                    
                    
                    
                }
                
            }
        }
        
        

}
    
    
    func getPathOfCSVFile(completionhandler:@escaping ()->Void) -> Void {
        Alamofire.request(CTSingleton.shared.exposure_site_website_link).responseString { response in
            print("\(response.result.isSuccess)")
            if let html_input = response.result.value {
                //self.parseHTML(html_input: html)
                let start_index_here = self.getSubStingStartIndex(string_input: html_input, sub_string_input: "Papa.")
                let end_index_here = self.getSubStingStartIndex(string_input: html_input, sub_string_input: ".csv")
                let test1 = html_input.index(html_input.startIndex, offsetBy: start_index_here + 12)
                let test2 = html_input.index(html_input.startIndex, offsetBy: start_index_here + (end_index_here - start_index_here + 3))
                CTSingleton.shared.csv_file_web_path = String(html_input[test1...test2])
                completionhandler()
            }
            else
            {
                
            }
        }
    }
    
    func parseHTML(html_input: String) -> Void {
        
        let start_index_here = getSubStingStartIndex(string_input: html_input, sub_string_input: "Papa.")
        let end_index_here = getSubStingStartIndex(string_input: html_input, sub_string_input: ".csv")
        let test1 = html_input.index(html_input.startIndex, offsetBy: start_index_here + 12)
        let test2 = html_input.index(html_input.startIndex, offsetBy: start_index_here + (end_index_here - start_index_here + 3))
        
        CTSingleton.shared.csv_file_web_path = String(html_input[test1...test2])
        
        
    }
    
    func dbUpdatedToday()->Bool
    {
        let last_updated_string = CTSingleton.shared.getStatTextForStatName(p_stat_name: "LAST_UPDATED").split(separator: ",")[1].dropFirst()
        let today_date_string = CTSingleton.shared.getCurrentDateStringInFormat()
        
        return last_updated_string.elementsEqual(today_date_string)
    }
    
    
    func readCSVFile(completionhandler:@escaping ()->Void)
    {
        CTSingleton.shared.processing_Queue.async {
            print("file set for reading")
            print(CTSingleton.shared.file_name)
            print(self.getLocalFilePath())
            let file_stream = InputStream(url: self.getLocalFilePath())
            //let stream = InputStream(fileAtPath: getLocalFilePath() as String)
            let csv = try! CSVReader(stream: file_stream!)
            while let row = csv.next() {
                //print("\(row)")
                //print("\(row)")
                var current_exposure_site = ExposureSite()
                current_exposure_site.LocationStatus = row[1]
                current_exposure_site.LocationName = row[2]
                current_exposure_site.Address = row[3]
                current_exposure_site.Suburb = row[4]
                current_exposure_site.State = row[5]
                let date_String = row[6].split(separator: "-", maxSplits: 3, omittingEmptySubsequences: false)
                current_exposure_site.Date = String(date_String[0])
                //print(current_exposure_site.Date)
                current_exposure_site.Day = String(date_String[1])
                //print(current_exposure_site.Day)
                current_exposure_site.TimeStart = row[7]
                current_exposure_site.TimeEnd = row[8]
                current_exposure_site.ContactType = row[9]
                CTSingleton.shared.Exposure_Sites.append(current_exposure_site)
            }
            completionhandler()
        }
    
    
}


}

