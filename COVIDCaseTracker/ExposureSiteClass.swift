//
//  ExposureSiteClass.swift
//  COVIDCaseTracker
//
//  Created by Vin on 30/8/21.
//  Copyright © 2021 Vin. All rights reserved.
//

import Foundation

struct ExposureSite {
    var LocationStatus = ""
    var LocationName = ""
    var Address = ""
    var Suburb = ""
    var State = ""
    var Date = ""
    var Day = ""
    var TimeStart = ""
    var TimeEnd = ""
    var ContactType = ""
    
}
