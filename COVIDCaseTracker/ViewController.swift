//
//  ViewController.swift
//  COVIDCaseTracker
//
//  Created by Vin on 29/8/21.
//  Copyright © 2021 Vin. All rights reserved.
//

import UIKit
import Kanna


class ViewController: UIViewController {

    
    
    func createLoadingLayer()
    {
        // Then define the float value for a full circle
        let full_circle = CGFloat.pi * 2
        //Then define the value for the starting point of the circle
        let staring_Angle = -CGFloat.pi / 2
        // Then create the path of the circle
        let circularPath = UIBezierPath(arcCenter: self.view.center, radius: 100, startAngle: staring_Angle, endAngle: full_circle, clockwise: true)
        CTSingleton.shared.loading_layer.path = circularPath.cgPath
        CTSingleton.shared.loading_layer.fillColor = UIColor.clear.cgColor
        CTSingleton.shared.loading_layer.lineCap = .round
        CTSingleton.shared.loading_layer.lineWidth = 10
        CTSingleton.shared.loading_layer.strokeColor = UIColor.black.cgColor
        CTSingleton.shared.loading_layer.strokeEnd = 0
        self.view.layer.addSublayer(CTSingleton.shared.loading_layer)
        
        
    }
    
    func createShapeLayerAnimation()
    {
        
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        //basicAnimation.fromValue = 0
        basicAnimation.toValue = 1
        basicAnimation.duration = CFTimeInterval(CTSingleton.shared.animation_duration)
        basicAnimation.fillMode = CAMediaTimingFillMode.forwards
        basicAnimation.isRemovedOnCompletion = false
        CTSingleton.shared.loading_layer.add(basicAnimation, forKey: "What")
    }
    
    func createTitleLabel()
    {
        let title_label = UILabel()
        title_label.text = "ACT Covid Tracker"
        title_label.textColor = UIColor.black
        title_label.textAlignment = .center
        title_label.font = UIFont.init(name: "Menlo", size: 30)
            //UIFont.boldSystemFont(ofSize: 30)
        title_label.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100)
        //title_label.center = self.view.topAnchor
        self.view.addSubview(title_label)
        
    }
    

    func createSubtitleLabel()
    {
        //let subtitle_label = UILabel()
        CTSingleton.shared.subtitle_label.text = ""
        CTSingleton.shared.subtitle_label.textColor = UIColor.black
        CTSingleton.shared.subtitle_label.textAlignment = .center
        CTSingleton.shared.subtitle_label.font = UIFont.init(name: "Menlo", size: 20)
        //UIFont.boldSystemFont(ofSize: 30)
        CTSingleton.shared.subtitle_label.frame = CGRect(x: 0, y: 20, width: self.view.frame.width, height: 100)
        //title_label.center = self.view.topAnchor
        self.view.addSubview(CTSingleton.shared.subtitle_label)
        
    }
    
    func createNewCaseLabel()
    {
        CTSingleton.shared.new_case_label.text = "X New Cases"
        CTSingleton.shared.new_case_label.textColor = UIColor.black
        CTSingleton.shared.new_case_label.textAlignment = .center
        CTSingleton.shared.new_case_label.font = UIFont.init(name: "Menlo", size: 20)
        //let ypos = CTSingleton.shared.subtitle_label.frame.maxY + CTSingleton.shared.subtitle_label.frame.height
        let ypos = CTSingleton.shared.subtitle_label.frame.height
        print("Y Position is \(ypos)")
        CTSingleton.shared.new_case_label.frame = CGRect(x: 0, y: ypos , width: self.view.frame.width, height: 100)
        //title_label.center = self.view.topAnchor
        self.view.addSubview(CTSingleton.shared.new_case_label)
        
    }
    
    
    func createActiveCaseLabel()
    {
        CTSingleton.shared.active_case_label.text = "X Active Cases"
        CTSingleton.shared.active_case_label.textColor = UIColor.black
        CTSingleton.shared.active_case_label.textAlignment = .center
        CTSingleton.shared.active_case_label.font = UIFont.init(name: "Menlo", size: 20)
        //let ypos = CTSingleton.shared.subtitle_label.frame.maxY + CTSingleton.shared.subtitle_label.frame.height
        let ypos = CTSingleton.shared.new_case_label.frame.maxY
        print("Y Position is \(ypos)")
        CTSingleton.shared.active_case_label.frame = CGRect(x: 0, y: ypos + 20 , width: self.view.frame.width, height: 100)
        //title_label.center = self.view.topAnchor
        self.view.addSubview(CTSingleton.shared.active_case_label)
        
    }
    
    func createNegativeTestResultLabel()
    {
        CTSingleton.shared.negative_test_result_label.text = "X Active Cases"
        CTSingleton.shared.negative_test_result_label.textColor = UIColor.black
        CTSingleton.shared.negative_test_result_label.textAlignment = .center
        CTSingleton.shared.negative_test_result_label.font = UIFont.init(name: "Menlo", size: 20)
        CTSingleton.shared.negative_test_result_label.numberOfLines = 2
        //let ypos = CTSingleton.shared.subtitle_label.frame.maxY + CTSingleton.shared.subtitle_label.frame.height
        let ypos = CTSingleton.shared.active_case_label.frame.maxY
        print("Y Position is \(ypos)")
        CTSingleton.shared.negative_test_result_label.frame = CGRect(x: 0, y: ypos + 20 , width: self.view.frame.width, height: 200)
        //title_label.center = self.view.topAnchor
        self.view.addSubview(CTSingleton.shared.negative_test_result_label)
        
    }
    
    func createTotalVaccinationsLabel()
    {
        CTSingleton.shared.total_vaccinations_label.text = "Total Vaccinations"
        CTSingleton.shared.total_vaccinations_label.textColor = UIColor.black
        CTSingleton.shared.total_vaccinations_label.textAlignment = .center
        CTSingleton.shared.total_vaccinations_label.font = UIFont.init(name: "Menlo", size: 20)
        //CTSingleton.shared.total_vaccinations_label.numberOfLines = 2
        //let ypos = CTSingleton.shared.subtitle_label.frame.maxY + CTSingleton.shared.subtitle_label.frame.height
        let ypos = CTSingleton.shared.negative_test_result_label.frame.maxY
        print("Y Position is \(ypos)")
        CTSingleton.shared.total_vaccinations_label.frame = CGRect(x: 0, y: ypos , width: self.view.frame.width, height: 200)
        //title_label.center = self.view.topAnchor
        self.view.addSubview(CTSingleton.shared.total_vaccinations_label)
        
    }
    
    func createLoadingScreenLabel()
    {
    
        CTSingleton.shared.loading_screen_label.text = "Loading"
        CTSingleton.shared.loading_screen_label.textColor = UIColor.black
        CTSingleton.shared.loading_screen_label.textAlignment = .left
        CTSingleton.shared.loading_screen_label.font = UIFont.boldSystemFont(ofSize: 20)
        CTSingleton.shared.loading_screen_label.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        CTSingleton.shared.loading_screen_label.center = self.view.center
        self.view.addSubview(CTSingleton.shared.loading_screen_label)
  
        
    }
    
    func createLoadingScreenLableAnimation()
    {
        let LoadingUpdatedisplayLink = CADisplayLink(target: self, selector: #selector(handleLoadingUpdate))
        LoadingUpdatedisplayLink.add(to: .main, forMode: RunLoop.Mode.default)
    
    }
    
    func createNewCaseLabelAnimation()
    {
        let NewCaseNumberdisplayLink = CADisplayLink(target: self, selector: #selector(handleNewCaseNumberUpdate))
        NewCaseNumberdisplayLink.add(to: .main, forMode: RunLoop.Mode.default)
        
    }
    
    func createActiveCaseLabelAnimation()
    {
        let ActiveCaseNumberdisplayLink = CADisplayLink(target: self, selector: #selector(handleActiveCaseNumberUpdate))
        ActiveCaseNumberdisplayLink.add(to: .main, forMode: RunLoop.Mode.default)
        
    }
    
    func createNegativeTestResultLabelAnimation()
    {
        let NegativeTestResultCaseNumberdisplayLink = CADisplayLink(target: self, selector: #selector(handleNegativeTestResultNumberUpdate))
        NegativeTestResultCaseNumberdisplayLink.add(to: .main, forMode: RunLoop.Mode.default)
        
    }
    
    func createTotalVaccinationsLabelAnimation()
    {
        let TotalVaccinationCaseNumberdisplayLink = CADisplayLink(target: self, selector: #selector(handleTotalVaccinationsNumberUpdate))
        TotalVaccinationCaseNumberdisplayLink.add(to: .main, forMode: RunLoop.Mode.default)
    }
    
    @objc func handleTotalVaccinationsNumberUpdate()
    {
        
        
        CTSingleton.shared.total_vaccinations_label.text = "\(CTSingleton.shared.total_vacinations_start_value) Total Vaccinations"
        
        
        CTSingleton.shared.processing_Queue.async {
            if ( CTSingleton.shared.total_vacinations_start_value != CTSingleton.shared.total_vaccinations && CTSingleton.shared.total_vacinations_start_value > 0 )
            {
                CTSingleton.shared.total_vacinations_start_value = CTSingleton.shared.total_vacinations_start_value - 1
            }
        }
       
        
    }
    
    @objc func handleNegativeTestResultNumberUpdate()
    {
        CTSingleton.shared.negative_test_result_label.text = "\(CTSingleton.shared.negative_test_result_start_value) Negative test results \n in the past 24 hours"
        
        CTSingleton.shared.processing_Queue.async
        {
            
            if ( CTSingleton.shared.negative_test_result_start_value != CTSingleton.shared.negative_test_results_in_24_hrs && CTSingleton.shared.negative_test_result_start_value > 0 )
            {
                CTSingleton.shared.negative_test_result_start_value = CTSingleton.shared.negative_test_result_start_value - 1
            }
    
        }
        
       
        
    }
    
    @objc func handleNewCaseNumberUpdate()
    {
        CTSingleton.shared.new_case_label.text = "\(CTSingleton.shared.new_daily_case_start_value) New Cases"
        
        CTSingleton.shared.processing_Queue.async
        {
            if ( CTSingleton.shared.new_daily_case_start_value != CTSingleton.shared.daily_new_cases && CTSingleton.shared.new_daily_case_start_value > 0  )
            {
                CTSingleton.shared.new_daily_case_start_value = CTSingleton.shared.new_daily_case_start_value - 1
            }
        }
        
        
        
    }
    
    @objc func handleActiveCaseNumberUpdate()
    {
        CTSingleton.shared.active_case_label.text = "\(CTSingleton.shared.active_case_start_value) Active Cases"

        CTSingleton.shared.processing_Queue.async
        {
            if ( CTSingleton.shared.active_case_start_value != CTSingleton.shared.active_cases && CTSingleton.shared.active_case_start_value > 0 )
            {
                CTSingleton.shared.active_case_start_value = CTSingleton.shared.active_case_start_value - 1
            }
        }
        

        
        
        
    }
    

    
    @objc func handleLoadingUpdate()
    {
        
        if (CTSingleton.shared.loading_screen_label.text == "Loading.....")
        {
            CTSingleton.shared.loading_screen_label.text = "Loading"
        }
        else
        {
            CTSingleton.shared.loading_screen_label.text = CTSingleton.shared.loading_screen_label.text!+"."
        }
        sleep(UInt32(1))
    
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //getPathOfCSVFile()
        
        createTitleLabel()
        createSubtitleLabel()
        createNewCaseLabel()
        createNewCaseLabelAnimation()
        createActiveCaseLabel()
        createActiveCaseLabelAnimation()
        createNegativeTestResultLabel()
        createNegativeTestResultLabelAnimation()
        createTotalVaccinationsLabel()
        createTotalVaccinationsLabelAnimation()
        
//        createLoadingLayer()
//        createLoadingScreenLabel()
//        createLoadingScreenLableAnimation()
//        createShapeLayerAnimation()
        
        
        CTSingleton.shared.downloadCSVFile {
            CTSingleton.shared.readCSVFile(completionhandler: {
//                CTSingleton.shared.loading_layer.strokeEnd = 1
                
//                test.strokeEnd = 1
//                sleep(UInt32(3))
                
                print("File read")
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3, execute: {
                    CTSingleton.shared.loading_layer.removeAllAnimations()
                    CTSingleton.shared.loading_layer.removeFromSuperlayer()
                    CTSingleton.shared.loading_screen_label.removeFromSuperview()
                })
                
                CTSingleton.shared.getDailyCOVID19Stats(completionhandler: {
                    
                    if(CTSingleton.shared.get_value_from_db)
                    {
                        print("VALUES UPDATED FROM DB")
                        DispatchQueue.main.async {
                            CTSingleton.shared.subtitle_label.text = CTSingleton.shared.getStatTextForStatName(p_stat_name: "LAST_UPDATED")
                            CTSingleton.shared.daily_new_cases = Int(CTSingleton.shared.getStatTextForStatName(p_stat_name: "NEW_DAILY_CASES"))!
                            CTSingleton.shared.active_cases = Int(CTSingleton.shared.getStatTextForStatName(p_stat_name: "ACTIVE_CASES"))!
                            //print(CTSingleton.shared.getStatTextForStatName(p_stat_name: "NEW_DAILY_CASES"))
                            //print(CTSingleton.shared.getStatTextForStatName(p_stat_name: "ACTIVE_CASES"))
                            //CTSingleton.shared.new_case_label.text = CTSingleton.shared.getStatTextForStatName(p_stat_name: "NEW_DAILY_CASES")
                            //CTSingleton.shared.active_case_label.text = CTSingleton.shared.getStatTextForStatName(p_stat_name: "ACTIVE_CASES")
                        }
                        
                    }
                    else
                    {
                        print("Values updated from Internet")
                        CTSingleton.shared.subtitle_label.text = CTSingleton.shared.last_updated
                        
                        //Only insert this into DB if the value does't exist
                        if (CTSingleton.shared.getStatTextForStatName(p_stat_name: "LAST_UPDATED") == "")
                        {
                            CTSingleton.shared.insertRowInTable(stat_name: "LAST_UPDATED", stat_text: CTSingleton.shared.last_updated)
                        }
                        
                        if (CTSingleton.shared.getStatTextForStatName(p_stat_name: "NEW_DAILY_CASES") == "")
                        {
                            CTSingleton.shared.insertRowInTable(stat_name: "NEW_DAILY_CASES", stat_text: String(CTSingleton.shared.daily_new_cases))
                        }
                        
                        if (CTSingleton.shared.getStatTextForStatName(p_stat_name: "ACTIVE_CASES") == "")
                        {
                            CTSingleton.shared.insertRowInTable(stat_name: "ACTIVE_CASES", stat_text: String(CTSingleton.shared.active_cases))
                        }
                        
                        
                        // If the value already exists in the DB but is out of date, we need to update the value and not insert it
                        if (CTSingleton.shared.getStatTextForStatName(p_stat_name: "LAST_UPDATED") != "" && CTSingleton.shared.getStatTextForStatName(p_stat_name: "LAST_UPDATED") != CTSingleton.shared.last_updated)
                        {
                            CTSingleton.shared.updateRowInTable(stat_name: "LAST_UPDATED", stat_text: CTSingleton.shared.last_updated)
                        }
                        
                        if (CTSingleton.shared.getStatTextForStatName(p_stat_name: "NEW_DAILY_CASES") != "" && CTSingleton.shared.getStatTextForStatName(p_stat_name: "NEW_DAILY_CASES") != String(CTSingleton.shared.daily_new_cases))
                        {
                            CTSingleton.shared.updateRowInTable(stat_name: "NEW_DAILY_CASES", stat_text: String(CTSingleton.shared.daily_new_cases))
                        }
                        
                        if (CTSingleton.shared.getStatTextForStatName(p_stat_name: "ACTIVE_CASES") != "" && CTSingleton.shared.getStatTextForStatName(p_stat_name: "ACTIVE_CASES") != String(CTSingleton.shared.daily_new_cases))
                        {
                            CTSingleton.shared.updateRowInTable(stat_name: "ACTIVE_CASES", stat_text: String(CTSingleton.shared.active_cases))
                        }
                        
                    }
                    
                    
                    
                    //Print all values in the table
                    //CTSingleton.shared.printAllRowsFromTable()
//                    CTSingleton.shared.printAllRowsFromTable()
//                    print(CTSingleton.shared.getStatTextForStatName(p_stat_name: "LAST_UPDATED"))
//                    print(CTSingleton.shared.updateRowInTable(stat_name: "LAST_UPDATED", stat_text: "asfsad"))
//                    CTSingleton.shared.printAllRowsFromTable()
                })
                
            })
            
//            for ES in self.Exposure_Sites
//            {
//                print(ES.LocationName)
//            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
    }
    
    


}
    
    
 
    


    
    




