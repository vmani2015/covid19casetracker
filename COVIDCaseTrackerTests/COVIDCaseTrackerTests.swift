//
//  COVIDCaseTrackerTests.swift
//  COVIDCaseTrackerTests
//
//  Created by Vin on 29/8/21.
//  Copyright © 2021 Vin. All rights reserved.
//

import XCTest
@testable import COVIDCaseTracker

class COVIDCaseTrackerTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

//    func testExample() {
//        //CTSingleton.shared.createandOpenDatabase()
//        CTSingleton.shared.printAllRowsFromTable()
//        //CTSingleton.shared.closeDatabase()
//
//        // This is an example of a functional test case.
//        // Use XCTAssert and related functions to verify your tests produce the correct results.
//    }
    
    func testAllRowsInDB()
    {
                CTSingleton.shared.createandOpenDatabase()
                CTSingleton.shared.printAllRowsFromTable()
                CTSingleton.shared.closeDatabase()
    }
    
    func testDateLastUpdate() {
        let date_string = CTSingleton.shared.getStatTextForStatName(p_stat_name: "LAST_UPDATED").split(separator: ",")[1].dropFirst()
        let date_string_split = date_string.split(separator: " ")
        let day = date_string_split[0]
        let month = date_string_split[1]
        let year = date_string_split[2]
        
        //Check that the "LAST_UPDATED" value can be sucessfully split into components
        XCTAssertEqual(date_string, "06 Sep 2021", "YES")
        XCTAssertEqual(day,"06", "YES")
        XCTAssertEqual(month,"Sep", "YES")
        XCTAssertEqual(year,"2021", "YES")
        
        
        
        
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testgetCurrentDateStringInFormat()
    {
        //Check that the get getCurrentDateStringInFormat() returns the current date in a dd MMM yyyy format
        // if no format is specifed ( dd MMM yyyy is the default format)
        XCTAssertEqual("07 Sep 2021", CTSingleton.shared.getCurrentDateStringInFormat(), "YES")
        
        //Check that the get getCurrentDateStringInFormat() returns the current date in a dd MonthName yyyy format
        XCTAssertEqual("07 September 2021", CTSingleton.shared.getCurrentDateStringInFormat(p_format: "dd MMMM yyyy"), "YES")
        
        //Wanted to see what would print with the expanded time zone name format.
        XCTAssertEqual("Australian Eastern Standard Time", CTSingleton.shared.getCurrentDateStringInFormat(p_format: "zzzz"), "YES")
        
       
    }
    
    

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
